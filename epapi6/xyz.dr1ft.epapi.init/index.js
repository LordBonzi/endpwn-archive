/*

    EPAPI6 Init

    Copyright 2018 EndPwn Project

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    DO NOT EDIT THIS FILE! Your bootstrap may overwrite changes to it, and you will lose your work!
    EndPwn3 users: You can prevent this by creating a file in the same directory named DONTUPDATE

    https://github.com/endpwn/

*/

const fs = require('fs');
const electron = require('electron').remote;
const app = electron.app;

const common = require('./common.js');

var alreadyInit = false;

var bootstrap;
var data;
var plugins;

function fetchPlugins() {

    // an array to hold all the plugins
    plugins = common.plugins = window.endpwn.plugins = [];

    // iterate over the plugins directory
    fs.readdirSync(data + '/plugins').forEach(x => {

        // get the plugin path and manifest path
        var path = `${data}/plugins/${x}`;
        var manifestPath = `${data}/plugins/${x}/manifest.json`;

        // make sure the plugin is a directory before trying to load it
        if (fs.lstatSync(path).isDirectory()) {

            // make sure there's a manifest
            // TODO: check for id conflicts
            if (fs.existsSync(manifestPath)) {

                var manifest;
                try {

                    // read the manifest and parse it
                    manifest = JSON.parse(fs.readFileSync(manifestPath).toString());

                    // the manifest *must* specify an id for the plugin or it will be discarded
                    if (!manifest || !manifest.id) {
                        common.debug(`the manifest for ${path} is invalid, ignoring...`);
                        return;
                    }

                }
                catch (ex) {
                    common.error(ex, `failed to load manifest for ${path}`);
                    manifest[k] = undefined;
                    return;
                }

                // insert the path to the plugin into its object
                manifest.path = path;

                // push the plugin into the plugin cache
                plugins.push(manifest);

            }
            else common.debug(`${path} has no manifest, ignoring...`);

        }
        else common.debug(`${path} is not a directory, ignoring...`);

    });

}

// you shouldnt mess with endpwn.loader unless you have a strong, strong understanding of how EPAPI6 works internally 
module.exports = {

    // the entrypoint for EPAPI6
    init: function (properties) {

        if (alreadyInit) throw 'already initialized';

        bootstrap = properties;
        data = bootstrap.data || app.getPath('userData');

        // the global table, used for storing and retrieving lots of important data
        var endpwn = {
            notice: 'Never, ever modify this object unless you know exactly what you are doing!',
            bootstrap: properties,
            loader: module.exports,
            data: data
        };

        // prevent old plugins from fucking the global table up
        Object.defineProperty(window, 'endpwn', {
            get: () => endpwn,
            set: () => common.warn('something tried to redefine window.endpwn, blocking...')
        });

        common.print(`bootstrap ${bootstrap.name} ${bootstrap.version} using ${bootstrap.method}`);
        common.print(`data path ${data}`);

        common.print('fetching plugins...');
        fetchPlugins();

        common.debug('resolving replacements and preload...');
        this.resolveAll(['replacements', 'preload']);

        common.print('loading core preloads...');
        this.loadGroup('preload', true);
        common.print('loading non-core preloads...');
        this.loadGroup('preload', false);

        // can be used to load the exports directive of other plugins
        window.mrequire = function (id) {
            return endpwn.loader.resolveKey(id, 'exports');
        }

        common.debug('resolving domready, main, and exports...');
        this.resolveAll(['domready', 'main', 'exports']);

        // prevent double loading
        alreadyInit = true;

    },

    // resolve a plugin id into a plugin object
    resolve: function (id) {
        var dependency = endpwn.plugins.filter(x => x.id == id)[0];
        if (!dependency) throw `could not resolve ${id}`;
        return dependency;
    },

    // keeps track of directives that have already been resolved
    resolved: {},

    // keeps track of entrypoints that have already been run
    loaded: {},

    // resolve a string (path) into an object (by requiring the path and overwriting the string with the exports)
    resolveKey: function (plugin, key) {

        if (typeof plugin == 'string') plugin = this.resolve(plugin);

        var resolved = this.resolved[key] || (this.resolved[key] = []);

        // already resolved
        if (resolved.includes(plugin.id)) return plugin[key];

        // skip if not defined
        if (!plugin[key]) return;

        // get the full path to the file described in the directive
        var fPath = `${plugin.path}/${plugin[key]}`;

        try {
            // load it
            plugin[key] = require(fPath);
        }
        catch (ex) {
            common.error(ex, `failed to resolve ${key} of ${fPath}`);
            plugin[key] = undefined;
        }

        resolved.push(plugin.id);

        return plugin[key];

    },

    // resolve all of a certain directive
    resolveAll: function (keys) {
        if (typeof keys == 'string') keys = [keys];
        endpwn.plugins.forEach(manifest =>
            keys.forEach(k => this.resolveKey(manifest, k)));
    },

    // execute an entrypoint on all plugins; basically where most of the magic happens
    load: function (plugin, key) {

        var loaded = this.loaded[key] || (this.loaded[key] = []);

        try {

            // already loaded
            if (loaded.includes(plugin.id)) return;

            // TODO: circular dependency detection
            if (plugin.depends) {
                plugin.depends.forEach(depend => {
                    if (!loaded.includes(depend)) {
                        common.debug(`${plugin.id} depends on ${depend} which is not yet loaded`);
                        var dependency = this.resolve(depend);
                        if (!dependency[key] || typeof dependency[key] != 'function') {
                            common.debug(`the dependency doesnt have a valid ${key}, ignoring`);
                            loaded.push(dependency.id);
                            return;
                        }
                        this.load(dependency, key);
                    }
                })
            }

            plugin[key]();
            loaded.push(plugin.id);
        }
        catch (ex) {
            common.error(ex, `${key} for ${plugin.id} failed`)
        }

    },

    // load a group of functions (like preload or main)
    loadGroup: function (key, core = false) {
        plugins.filter(x => (core ? x.core : !x.core) && x[key] && typeof x[key] == 'function')
            .forEach(plugin => this.load(plugin, key));
    }

}
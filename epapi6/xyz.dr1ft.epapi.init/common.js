/*

    EPAPI6 Init

    Copyright 2018 EndPwn Project

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    DO NOT EDIT THIS FILE! Your bootstrap may overwrite changes to it, and you will lose your work!
    EndPwn3 users: You can prevent this by creating a file in the same directory named DONTUPDATE

    https://github.com/endpwn/

*/

module.exports = {

    // printing functions
    debug: function (t) {
        console.debug(`%c[Init]%c ${t}`, 'font-weight:bold;color:#0cc', '');
    },
    print: function (t) {
        console.log(`%c[Init]%c ${t}`, 'font-weight:bold;color:#0cc', '');
    },
    warn: function (t) {
        console.warn(`%c[Init]%c ${t}`, 'font-weight:bold;color:#0cc', '');
    },
    error: function (e, t) {
        if (typeof (t) == 'undefined') t = 'uncaught exception';
        console.error(`%c[Init]%c ${t}:\n\n`, 'font-weight:bold;color:#0cc', '', e);
    }

}
/*

    EndPwn Customizer
    
    Copyright 2018 EndPwn Project
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    DO NOT EDIT THIS FILE! Your bootstrap may overwrite changes to it, and you will lose your work!
    EndPwn3 users: You can prevent this by creating a file in the same directory named DONTUPDATE

    https://github.com/endpwn/

*/

const util = mrequire('xyz.dr1ft.epapi.core');
const events = mrequire('xyz.dr1ft.epapi.events');
const internal = mrequire('xyz.dr1ft.epapi.internal');

function print(str) {
    console.log(`%c[Customizer]%c ` + str, 'font-weight:bold;color:#0cc', '');
}

module.exports = function () {

    endpwn.customizer = {

        // fallback data
        data: {
            guilds: [],
            devs: [],
            bots: [],
            users: {}
        },

        get me() {
            return {
                bot: this.data.bots.indexOf(internal.getId()) != -1,
                discrim: this.data.users[internal.getId()]
            }
        },

        update: function () {
            // fetch goodies.json
            print('fetching data from server...');
            fetch('https://endpwn.cathoderay.tube/goodies.json?_=' + Date.now())
                .then(x => x.json())
                .then(r => this.data = r);
        }

    };

    // apply custom discrims/bot tags/badges/server verif from EndPwn Customizer (endpwn.cathoderay.tube)

    // refetch customizer stuff every half hour
    setInterval(endpwn.customizer.update, 1800000);
    endpwn.customizer.update();

    // add the endpwn dev badge to the class obfuscation table
    wc.findFunc('profileBadges:"profileBadges')[0].exports['profileBadgeEndpwn'] = 'profileBadgeEndPwn';

    // hook getUser() so we can apply custom discrims/bot tags/badges
    util.wrapAfter(
        "wc.findCache('getUser')[0].exports.getUser",

        x => {

            if (x === undefined || x === null) return;

            if (endpwn.customizer.data.bots.includes(x.id)) x.bot = true;
            if (endpwn.customizer.data.users[x.id] !== undefined) x.discriminator = endpwn.customizer.data.users[x.id];
            if (endpwn.customizer.data.devs.includes(x.id)) x.flags += x.flags & 4096 ? 0 : 4096;

            return x;
        }
    );

    // make sure devs' badges actually render
    events.hook('USER_PROFILE_MODAL_FETCH_SUCCESS', x => { if (endpwn.customizer.data.devs.includes(x.user.id)) x.user.flags += x.user.flags & 4096 ? 0 : 4096; })

    // hook getGuild() so we can verify servers
    util.wrapAfter(
        "wc.findCache('getGuild')[0].exports.getGuild",

        x => {

            if (x === undefined || x === null) return;

            if (endpwn.customizer.data.guilds.includes(x.id)) x.features.add('VERIFIED');

            return x;
        }
    );

}